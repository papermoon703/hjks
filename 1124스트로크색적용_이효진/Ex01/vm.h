#include <stdio.h>
#include <stdlib.h>
#include <math.h>


//typedef float vector3f[3];
typedef float matrix2f[2][2];
typedef float matrix3f[3][3];
typedef float V3DF[3];
//typedef float vector3f[3];

//vector 함수
void vector (V3DF v1, float a, float b, float c);
void vadd3f (V3DF a, V3DF b, V3DF c);
void vcross3f (V3DF a, V3DF b, V3DF c);
float vdot3f (V3DF a, V3DF b);
bool vequal3f (V3DF a, V3DF b);
//void vnorm3f (V3DF a);

//matrix 함수
void matrix (matrix3f x, float a, float b, float c, float d, float e, float f, float g, float h, float i);
void mmult3f (matrix3f a, matrix3f b, matrix3f c);
float mdet2f (matrix2f a); 
float mdet3f (matrix3f a);
void mtranspose3f (matrix3f a, matrix3f b); 

//vector * matrix 함수
void vmmult3f (V3DF a, V3DF b, matrix3f c); 
void mvmult3f (V3DF a, matrix3f b, V3DF c); 