//	Image.cpp

#include "Image.h"
#include "vm.h"

float r2 = 0.0f;
float s2 = 1.0f;

int index=0;
extern int Width;
extern int Height;
int num=0;
int num3=80099;
int d=100;
extern V3DF White, LightGray, Gray, DarkGray, Black, Red, Green, Blue, LightBlue, Cyan, Violet, Yellow;
extern V3DF LightRed, LightGreen, LightBlue, LightCyan, LightViolet, LightYellow, DarkRed, DarkGreen, DarkBlue, DarkCyan, DarkViolet, DarkYellow;
extern V4DF TYellow;
extern GLfloat light0_ambient[], light0_diffuse[], light0_position[];
extern GLfloat light1_ambient[], light1_diffuse[], light1_position[];
extern GLfloat ambient_diffuse1[], ambient_diffuse2[], ambient_diffuse3[]; 
extern GLfloat no_ambient[], no_diffuse[], no_specular[], no_emission[], no_shininess[];
extern void pushTexture2 (int index);
extern void vector3f ( V3DF v, float v1, float v2, float v3 );
extern void save_bitmap_file (char *fn, int W, int H, GLubyte *rpxls);
extern Image canvas;
extern Image brush;
extern void hk_display();

void draw_rectangle ( V3DF p1, V3DF p2, V3DF p3, V3DF p4, V3DF clr, float wid )
{
	glColor3fv ( clr );
	glLineWidth ( wid );
	glBegin ( GL_LINE_LOOP );
		glVertex3fv ( p1 );
		glVertex3fv ( p2 );
		glVertex3fv ( p3 );
		glVertex3fv ( p4 );
	glEnd ( );
}

/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@      PUBLIC METHODS     @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/

Image::Image()
{
}

Image::~Image()
{
	if (cim){
		delete[]cim;
	}
	if (im){
		delete[]im;
	}
}

bool Image::loadBitmap2 ( char *fn )
{
	printf("load....\n");
	V3DUC temp;
	Bitmap *image; 

	FILE *fp = fopen ( fn, "r+b" );
	if ( fp == NULL )
		return false;

	image=new Bitmap();

	if(image==NULL) {
		return false;
	}

	if (image->loadBMP(fn)) {
		Width = image->width;
		Height = image->height;
		MAX_X = Width;
		MAX_Y = Height;

		int i, j;
		int idx;			
		Ncim = (V3DF *) calloc ( image->width * image->height, sizeof(V3DF) );
		cim = (V3DF *) calloc ( image->width * image->height, sizeof(V3DF) );
		im = (float *) calloc ( image->width * image->height, sizeof(float) );

		for ( i = 0; i < image->width; i++ ) {
			for ( j = 0; j < image->height; j++ ) {
				idx = 3*(j * image->width + i);
				temp[0] = image->data[idx];
				temp[1] = image->data[idx+1];
				temp[2] = image->data[idx+2];
				vector3f ( cim[i+j*image->width], (float) temp[0]/255.0f, (float) temp[1]/255.0f, (float) temp[2]/255.0f );
				im[i+j*image->width] = cim[i+j*image->width][0]*RED_WEIGHT + cim[i+j*image->width][1]*GREEN_WEIGHT + cim[i+j*image->width][2]*BLUE_WEIGHT;
			}
		}
	}
	else {
		return false;
	}
	for ( int i = 0; i < image->width; i++ ) {
			for ( int j = 0; j < image->height; j++ ) {
				Ncim[i+MAX_X*j][0] = cim[i+MAX_X*j][0];
				Ncim[i+MAX_X*j][1] = cim[i+MAX_X*j][1];
				Ncim[i+MAX_X*j][2] = cim[i+MAX_X*j][2];
			}
	}

	return true;
}

void Image::draw ( )
{
	int i, j;
	for ( i = 0; i < MAX_X; i++ ) {
		for ( j = 0; j < MAX_Y; j++ ) {
			glColor3f ( Ncim[i+j*MAX_X][0], Ncim[i+j*MAX_X][1], Ncim[i+j*MAX_X][2] );
			glRecti ( i, j, i+1, j+1 );
		}
	}
}

void FLOATtoBMP2 ( char *fn, int Width, int Height, float *InImg )
{
	int k;
	int m,n;
	GLubyte* Bits = new GLubyte[Width*Height*3];
	memset(Bits, 0,Width*Height*3);
	for (k=0; k<Width*Height*3; k=k+3){
		m = (k/3)%Width;
		n = k/(Width*3);
		Bits[k] = (GLubyte)(InImg[k/3]*255);
		Bits[k+1] = (GLubyte)(InImg[k/3]*255);
		Bits[k+2] = (GLubyte)(InImg[k/3]*255);
	}

	save_bitmap_file(fn, Width, Height, Bits);
	delete []Bits;
}


void vertexV(V3DF a, V3DF b, V3DF c, V3DF d, int SPx, int SPy)
{
	a[0] = SPx;
	a[1] = SPy;
	a[2] = 1.0;

	b[0] = SPx;
	b[1] = SPy+1;
	b[2] = 1.0;

	c[0] = SPx+1;
	c[1] = SPy;
	c[2] = 1.0;

	d[0] = SPx+1;
	d[1] = SPy+1;
	d[2] = 1.0;
}

float Extent(V3DF a, V3DF b) {
	float Width1;
	float Height1;
    
	Width1 = a[0]-b[0];
	Height1 = a[1]-b[1];

	if(Width1<0)
		Width1 *= -1;
	if(Height1<0)
		Height1 *= -1;

	return Width1*Height1;
}

void Image::rotation2 ( float r, float width, float height )
{
	V3DF Ov;
	V3DF Iv;
	int SPx,SPy;
	V3DF VP1;
	V3DF VP2;
	V3DF VP3;
	V3DF VP4;

	r2 += r; // 각도를 계속 증가시키기 위해

	//=============================R, T 행렬 정의=================================
	//R정의
	matrix3f rotate; 
	matrix (rotate, 
				cos(3.1415926/180*r2), -sin(3.1415926/180*r2), 0.0, 
				sin(3.1415926/180*r2), cos(3.1415926/180*r2), 0.0, 
				0.0, 0.0, 1.0 );
	//T 정의
	matrix3f trans;
	matrix ( trans, 
				1.0, 0.0, -(width/2), 
				0.0, 1.0, -(height/2), 
				0.0, 0.0, 1.0 );
	//T의 Inverse 정의
	float det;
	matrix3f transIn;
	matrix (transIn, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

	det = mdet3f(trans);
	if(det==0) {
		printf("역행렬이 없습니다. \n");
	    return;
	}
	else {
		//역행렬만들기 위한 2차원 행렬 선언부분
		matrix2f Im00 = {trans[1][1],trans[1][2],trans[2][1],trans[2][2]};
		matrix2f Im01 = {trans[1][0],trans[1][2],trans[2][0],trans[2][2]};
		matrix2f Im02 = {trans[1][0],trans[1][1],trans[2][0],trans[2][1]};
		matrix2f Im10 = {trans[0][1],trans[0][2],trans[2][1],trans[2][2]};
		matrix2f Im11 = {trans[0][0],trans[0][2],trans[2][0],trans[2][2]};
		matrix2f Im12 = {trans[0][0],trans[0][1],trans[2][0],trans[2][1]};
		matrix2f Im20 = {trans[0][1],trans[0][2],trans[1][1],trans[1][2]};
		matrix2f Im21 = {trans[0][0],trans[0][2],trans[1][0],trans[1][2]};
		matrix2f Im22 = {trans[0][0],trans[0][1],trans[1][0],trans[1][1]};

		transIn[0][0] = mdet2f(Im00)/mdet3f(trans);
		transIn[0][1] = -mdet2f(Im10)/mdet3f(trans);
		transIn[0][2] = mdet2f(Im20)/mdet3f(trans);
		transIn[1][0] = -mdet2f(Im01)/mdet3f(trans);
		transIn[1][1] = mdet2f(Im11)/mdet3f(trans);
		transIn[1][2] = -mdet2f(Im21)/mdet3f(trans);
		transIn[2][0] = mdet2f(Im02)/mdet3f(trans);
		transIn[2][1] = -mdet2f(Im12)/mdet3f(trans);
		transIn[2][2] = mdet2f(Im22)/mdet3f(trans);
	}
	//===========================================TIn*R*T -> result에 최종 저장 ========================
	matrix3f result;
	mmult3f (result, rotate, trans);
	mmult3f (result, transIn, result);
	//===========================================기존이미지 하얀색으로 초기화============================
	for(int i=0; i<MAX_X; i++) {
		for(int j=0; j<MAX_Y; j++) {
			Ncim[i+MAX_X*j][0]=1;
			Ncim[i+MAX_X*j][1]=1;
			Ncim[i+MAX_X*j][2]=1;
		}
	}
	//=========================================result의 역행렬구하기->resultIn===========================
	float det2;
	matrix3f resultIn;
	matrix (resultIn, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

	det2 = mdet3f(result);
	if(det2==0) 
	{
		printf("역행렬이 없습니다. \n");
	    return;
	}
	else 
	{
		//역행렬만들기 위한 2차원 행렬 선언부분
		matrix2f Im200 = {result[1][1],result[1][2],result[2][1],result[2][2]};
		matrix2f Im201 = {result[1][0],result[1][2],result[2][0],result[2][2]};
		matrix2f Im202 = {result[1][0],result[1][1],result[2][0],result[2][1]};
		matrix2f Im210 = {result[0][1],result[0][2],result[2][1],result[2][2]};
		matrix2f Im211 = {result[0][0],result[0][2],result[2][0],result[2][2]};
		matrix2f Im212 = {result[0][0],result[0][1],result[2][0],result[2][1]};
		matrix2f Im220 = {result[0][1],result[0][2],result[1][1],result[1][2]};
		matrix2f Im221 = {result[0][0],result[0][2],result[1][0],result[1][2]};
		matrix2f Im222 = {result[0][0],result[0][1],result[1][0],result[1][1]};

		resultIn[0][0] = mdet2f(Im200)/mdet3f(result);
		resultIn[0][1] = -mdet2f(Im210)/mdet3f(result);
		resultIn[0][2] = mdet2f(Im220)/mdet3f(result);
		resultIn[1][0] = -mdet2f(Im201)/mdet3f(result);
		resultIn[1][1] = mdet2f(Im211)/mdet3f(result);
		resultIn[1][2] = -mdet2f(Im221)/mdet3f(result);
		resultIn[2][0] = mdet2f(Im202)/mdet3f(result);
		resultIn[2][1] = -mdet2f(Im212)/mdet3f(result);
		resultIn[2][2] = mdet2f(Im222)/mdet3f(result);
	}

	//=========================================보간법을 이용해 벡터에적용시키기===========================
	for(int i=0; i<MAX_X; i++) {
		for(int j=0; j<MAX_Y; j++) {
				vector(Ov,(float)i,(float)j,1.0); 
				mvmult3f (Iv, resultIn, Ov);
				SPx = (int)floor(Iv[0]);
				SPy = (int)floor(Iv[1]);
				if(((SPx>0) && (SPx<MAX_X)) && ((SPy>0) && (SPy<MAX_Y)) && ((SPx) + MAX_X*(SPy+1)<(MAX_X*MAX_Y))) {
					vertexV(VP1, VP2, VP3, VP4, SPx, SPy); 
					Ncim[i+MAX_X*j][0] = (float)(cim[(SPx) + MAX_X*(SPy)][0] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][0] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][0] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][0] * Extent(Iv, VP1));
					Ncim[i+MAX_X*j][1] = (float)(cim[(SPx) + MAX_X*(SPy)][1] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][1] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][1] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][1] * Extent(Iv, VP1));
					Ncim[i+MAX_X*j][2] = (float)(cim[(SPx) + MAX_X*(SPy)][2] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][2] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][2] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][2] * Extent(Iv, VP1));
				}
				else {
					Ncim[i+MAX_X*j][0] = (float)1;
					Ncim[i+MAX_X*j][1] = (float)1;
					Ncim[i+MAX_X*j][2] = (float)1;
				}
		}
	}
}

void Image::rotation ( float *r, int index, float width, float height )
{
	V3DF Ov;
	V3DF Iv;
	int SPx,SPy;
	V3DF VP1;
	V3DF VP2;
	V3DF VP3;
	V3DF VP4;
	
	//for(int i=100; i<width; i+=d)
	//{
	//	for(int j=100; j<height; j+=d)
	//	{
				 // 각도를 계속 증가시키기 위해
			//	printf("%f", r2);
			//	//printf("%f", sctem);
				//=============================R, T 행렬 정의=================================
				//R정의
				matrix3f rotate; 
				matrix (rotate, 
							cos(3.1415926/180*r[index]), -sin(3.1415926/180*r[index]), 0.0, 
							sin(3.1415926/180*r[index]), cos(3.1415926/180*r[index]), 0.0, 
							0.0, 0.0, 1.0 );

				matrix3f trans;
				matrix ( trans, 
							1.0, 0.0, -(width/2), 
							0.0, 1.0, -(height/2), 
							0.0, 0.0, 1.0 );
				//T의 Inverse 정의
				float det;
				matrix3f transIn;
				matrix (transIn, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

				det = mdet3f(trans);
				if(det==0) 
				{
					printf("역행렬이 없습니다. \n");
					return;
				}
				else 
				{
					//역행렬만들기 위한 2차원 행렬 선언부분
					matrix2f Im00 = {trans[1][1],trans[1][2],trans[2][1],trans[2][2]};
					matrix2f Im01 = {trans[1][0],trans[1][2],trans[2][0],trans[2][2]};
					matrix2f Im02 = {trans[1][0],trans[1][1],trans[2][0],trans[2][1]};
					matrix2f Im10 = {trans[0][1],trans[0][2],trans[2][1],trans[2][2]};
					matrix2f Im11 = {trans[0][0],trans[0][2],trans[2][0],trans[2][2]};
					matrix2f Im12 = {trans[0][0],trans[0][1],trans[2][0],trans[2][1]};
					matrix2f Im20 = {trans[0][1],trans[0][2],trans[1][1],trans[1][2]};
					matrix2f Im21 = {trans[0][0],trans[0][2],trans[1][0],trans[1][2]};
					matrix2f Im22 = {trans[0][0],trans[0][1],trans[1][0],trans[1][1]};

					transIn[0][0] = mdet2f(Im00)/mdet3f(trans);
					transIn[0][1] = -mdet2f(Im10)/mdet3f(trans);
					transIn[0][2] = mdet2f(Im20)/mdet3f(trans);
					transIn[1][0] = -mdet2f(Im01)/mdet3f(trans);
					transIn[1][1] = mdet2f(Im11)/mdet3f(trans);
					transIn[1][2] = -mdet2f(Im21)/mdet3f(trans);
					transIn[2][0] = mdet2f(Im02)/mdet3f(trans);
					transIn[2][1] = -mdet2f(Im12)/mdet3f(trans);
					transIn[2][2] = mdet2f(Im22)/mdet3f(trans);
				}
				//===========================================TIn*R*T -> result에 최종 저장 ========================
				matrix3f mid, result;
				mmult3f (mid, rotate, trans);
				mmult3f (result, transIn, mid);
				//===========================================기존이미지 하얀색으로 초기화============================
				for(int i=0; i<MAX_X; i++)
				{
					for(int j=0; j<MAX_Y; j++) 
					{
						Ncim[i+MAX_X*j][0]=1;
						Ncim[i+MAX_X*j][1]=1;
						Ncim[i+MAX_X*j][2]=1;
					}
				}
				//=========================================result의 역행렬구하기->resultIn===========================
				float det2;
				matrix3f resultIn;
				matrix (resultIn, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

				det2 = mdet3f(result);
				if(det2==0) 
				{
					printf("역행렬이 없습니다. \n");
					return;
				}
				else 
				{
					//역행렬만들기 위한 2차원 행렬 선언부분
					matrix2f Im200 = {result[1][1],result[1][2],result[2][1],result[2][2]};
					matrix2f Im201 = {result[1][0],result[1][2],result[2][0],result[2][2]};
					matrix2f Im202 = {result[1][0],result[1][1],result[2][0],result[2][1]};
					matrix2f Im210 = {result[0][1],result[0][2],result[2][1],result[2][2]};
					matrix2f Im211 = {result[0][0],result[0][2],result[2][0],result[2][2]};
					matrix2f Im212 = {result[0][0],result[0][1],result[2][0],result[2][1]};
					matrix2f Im220 = {result[0][1],result[0][2],result[1][1],result[1][2]};
					matrix2f Im221 = {result[0][0],result[0][2],result[1][0],result[1][2]};
					matrix2f Im222 = {result[0][0],result[0][1],result[1][0],result[1][1]};

					resultIn[0][0] = mdet2f(Im200)/mdet3f(result);
					resultIn[0][1] = -mdet2f(Im210)/mdet3f(result);
					resultIn[0][2] = mdet2f(Im220)/mdet3f(result);
					resultIn[1][0] = -mdet2f(Im201)/mdet3f(result);
					resultIn[1][1] = mdet2f(Im211)/mdet3f(result);
					resultIn[1][2] = -mdet2f(Im221)/mdet3f(result);
					resultIn[2][0] = mdet2f(Im202)/mdet3f(result);
					resultIn[2][1] = -mdet2f(Im212)/mdet3f(result);
					resultIn[2][2] = mdet2f(Im222)/mdet3f(result);
				}

				//=========================================보간법을 이용해 벡터에적용시키기===========================

				for(int i=0; i<MAX_X; i++)
				{
					for(int j=0; j<MAX_Y; j++)
					{
							vector(Ov,(float)i,(float)j,1.0); 
							mvmult3f (Iv, resultIn, Ov);
							SPx = (int)floor(Iv[0]);
							SPy = (int)floor(Iv[1]);
							if(((SPx>0) && (SPx<MAX_X)) && ((SPy>0) && (SPy<MAX_Y)) && ((SPx) + MAX_X*(SPy+1)<(MAX_X*MAX_Y))) {
								vertexV(VP1, VP2, VP3, VP4, SPx, SPy); 
								Ncim[i+MAX_X*j][0] = (float)(cim[(SPx) + MAX_X*(SPy)][0] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][0] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][0] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][0] * Extent(Iv, VP1));
								Ncim[i+MAX_X*j][1] = (float)(cim[(SPx) + MAX_X*(SPy)][1] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][1] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][1] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][1] * Extent(Iv, VP1));
								Ncim[i+MAX_X*j][2] = (float)(cim[(SPx) + MAX_X*(SPy)][2] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][2] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][2] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][2] * Extent(Iv, VP1));
							}
							else 
							{
								Ncim[i+MAX_X*j][0] = (float)1;
								Ncim[i+MAX_X*j][1] = (float)1;
								Ncim[i+MAX_X*j][2] = (float)1;
							}
					}
				}
			if ( canvas.Ncim[index][0] == canvas.Ncim[320400][0] &&  canvas.Ncim[index][1] == 1.000000 && canvas.Ncim[index][2] == 0.000000 ){
				//먼저 scale -> rotate 해야되
				compositionSR2 ( r[index], index, width, height, 0.3 );
				pushTexture2 (index);
			}
			else
				pushTexture2 (index);


}
void Image::scaling2 ( float s , float width, float height )
{
	V3DF Ov;
	V3DF Iv;
	int SPx,SPy;
	V3DF VP1;
	V3DF VP2;
	V3DF VP3;
	V3DF VP4;

	//if ( s2 == 0.0 ){
	//	printf("0됨\n");
	//	s2 += s;
	//}
	//else
	//	s2 -= s;
	s2 = s;

	//printf("s2 = %f , s = %f\n" , s2 , s );
	//S 정의
	matrix3f scale;
	matrix (scale, 
					s2, 0.0, 0.0, 
					0.0, s2, 0.0, 
					0.0, 0.0, 1.0);
	//T 정의
	matrix3f trans;
	matrix ( trans, 
				1.0, 0.0, -(width/2), 
				0.0, 1.0, -(height/2), 
				0.0, 0.0, 1.0 );
	//T의 Inverse 정의
	float det;
	matrix3f transIn;
	matrix (transIn, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

	det = mdet3f(trans);
	if(det==0) {
		printf("역행렬이 없습니다. \n");
	    return;
	}
	else {
		//역행렬만들기 위한 2차원 행렬 선언부분
		matrix2f Im00 = {trans[1][1],trans[1][2],trans[2][1],trans[2][2]};
		matrix2f Im01 = {trans[1][0],trans[1][2],trans[2][0],trans[2][2]};
		matrix2f Im02 = {trans[1][0],trans[1][1],trans[2][0],trans[2][1]};
		matrix2f Im10 = {trans[0][1],trans[0][2],trans[2][1],trans[2][2]};
		matrix2f Im11 = {trans[0][0],trans[0][2],trans[2][0],trans[2][2]};
		matrix2f Im12 = {trans[0][0],trans[0][1],trans[2][0],trans[2][1]};
		matrix2f Im20 = {trans[0][1],trans[0][2],trans[1][1],trans[1][2]};
		matrix2f Im21 = {trans[0][0],trans[0][2],trans[1][0],trans[1][2]};
		matrix2f Im22 = {trans[0][0],trans[0][1],trans[1][0],trans[1][1]};

		transIn[0][0] = mdet2f(Im00)/mdet3f(trans);
		transIn[0][1] = -mdet2f(Im10)/mdet3f(trans);
		transIn[0][2] = mdet2f(Im20)/mdet3f(trans);
		transIn[1][0] = -mdet2f(Im01)/mdet3f(trans);
		transIn[1][1] = mdet2f(Im11)/mdet3f(trans);
		transIn[1][2] = -mdet2f(Im21)/mdet3f(trans);
		transIn[2][0] = mdet2f(Im02)/mdet3f(trans);
		transIn[2][1] = -mdet2f(Im12)/mdet3f(trans);
		transIn[2][2] = mdet2f(Im22)/mdet3f(trans);
	}
	//T-1*S*T
	matrix3f result;
	mmult3f (result, scale, trans);
	mmult3f (result, transIn, result);

	for(int i=0; i<MAX_X; i++) {
		for(int j=0; j<MAX_Y; j++) {
			Ncim[i+MAX_X*j][0]=1;
			Ncim[i+MAX_X*j][1]=1;
			Ncim[i+MAX_X*j][2]=1;
		}
	}

	//result 역행렬
	float det2;
	matrix3f resultIn;
	matrix (resultIn, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

	det2 = mdet3f(result);
	if(det2==0) 
	{
		printf("역행렬이 없습니다. \n");
	    return;
	}
	else 
	{
		//역행렬만들기 위한 2차원 행렬 선언부분
		matrix2f Im200 = {result[1][1],result[1][2],result[2][1],result[2][2]};
		matrix2f Im201 = {result[1][0],result[1][2],result[2][0],result[2][2]};
		matrix2f Im202 = {result[1][0],result[1][1],result[2][0],result[2][1]};
		matrix2f Im210 = {result[0][1],result[0][2],result[2][1],result[2][2]};
		matrix2f Im211 = {result[0][0],result[0][2],result[2][0],result[2][2]};
		matrix2f Im212 = {result[0][0],result[0][1],result[2][0],result[2][1]};
		matrix2f Im220 = {result[0][1],result[0][2],result[1][1],result[1][2]};
		matrix2f Im221 = {result[0][0],result[0][2],result[1][0],result[1][2]};
		matrix2f Im222 = {result[0][0],result[0][1],result[1][0],result[1][1]};

		resultIn[0][0] = mdet2f(Im200)/mdet3f(result);
		resultIn[0][1] = -mdet2f(Im210)/mdet3f(result);
		resultIn[0][2] = mdet2f(Im220)/mdet3f(result);
		resultIn[1][0] = -mdet2f(Im201)/mdet3f(result);
		resultIn[1][1] = mdet2f(Im211)/mdet3f(result);
		resultIn[1][2] = -mdet2f(Im221)/mdet3f(result);
		resultIn[2][0] = mdet2f(Im202)/mdet3f(result);
		resultIn[2][1] = -mdet2f(Im212)/mdet3f(result);
		resultIn[2][2] = mdet2f(Im222)/mdet3f(result);
	}

	//=========================================보간법을 이용해 벡터에적용시키기===========================
	for(int i=0; i<MAX_X; i++) {
		for(int j=0; j<MAX_Y; j++) {
				vector(Ov,(float)i,(float)j,1.0); 
				mvmult3f (Iv, resultIn, Ov);
				SPx = (int)floor(Iv[0]);
				SPy = (int)floor(Iv[1]);
				if(((SPx>0) && (SPx<MAX_X)) && ((SPy>0) && (SPy<MAX_Y)) && ((SPx) + MAX_X*(SPy+1)<(MAX_X*MAX_Y))) {
					vertexV(VP1, VP2, VP3, VP4, SPx, SPy); 
					Ncim[i+MAX_X*j][0] = (float)(cim[(SPx) + MAX_X*(SPy)][0] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][0] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][0] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][0] * Extent(Iv, VP1));
					Ncim[i+MAX_X*j][1] = (float)(cim[(SPx) + MAX_X*(SPy)][1] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][1] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][1] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][1] * Extent(Iv, VP1));
					Ncim[i+MAX_X*j][2] = (float)(cim[(SPx) + MAX_X*(SPy)][2] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][2] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][2] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][2] * Extent(Iv, VP1));
				   
				}
				else {
					Ncim[i+MAX_X*j][0] = (float)1;
					Ncim[i+MAX_X*j][1] = (float)1;
					Ncim[i+MAX_X*j][2] = (float)1;
				}
		}
	}
}
void Image::scaling ()
{
	matrix3f rbg; 
	V3DF Ov;
	V3DF Iv;
	//k += a;
	matrix (rbg, 0.7, 0.0, 0.0, 0.0, 0.7, 0.0, 0.0, 0.0, 1.0);
	
	float det;
	matrix3f rbgIn;
	matrix (rbgIn, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

	det = mdet3f(rbg);
	if(det==0) {
		printf("역행렬이 없습니다. \n");
	    return;
	}
	else {
		//역행렬만들기 위한 2차원 행렬 선언부분
		matrix2f Im00 = {rbg[1][1],rbg[1][2],rbg[2][1],rbg[2][2]};
		matrix2f Im01 = {rbg[1][0],rbg[1][2],rbg[2][0],rbg[2][2]};
		matrix2f Im02 = {rbg[1][0],rbg[1][1],rbg[2][0],rbg[2][1]};
		matrix2f Im10 = {rbg[0][1],rbg[0][2],rbg[2][1],rbg[2][2]};
		matrix2f Im11 = {rbg[0][0],rbg[0][2],rbg[2][0],rbg[2][2]};
		matrix2f Im12 = {rbg[0][0],rbg[0][1],rbg[2][0],rbg[2][1]};
		matrix2f Im20 = {rbg[0][1],rbg[0][2],rbg[1][1],rbg[1][2]};
		matrix2f Im21 = {rbg[0][0],rbg[0][2],rbg[1][0],rbg[1][2]};
		matrix2f Im22 = {rbg[0][0],rbg[0][1],rbg[1][0],rbg[1][1]};

		rbgIn[0][0] = mdet2f(Im00)/mdet3f(rbg);
		rbgIn[0][1] = -mdet2f(Im10)/mdet3f(rbg);
		rbgIn[0][2] = mdet2f(Im20)/mdet3f(rbg);
		rbgIn[1][0] = -mdet2f(Im01)/mdet3f(rbg);
		rbgIn[1][1] = mdet2f(Im11)/mdet3f(rbg);
		rbgIn[1][2] = -mdet2f(Im21)/mdet3f(rbg);
		rbgIn[2][0] = mdet2f(Im02)/mdet3f(rbg);
		rbgIn[2][1] = -mdet2f(Im12)/mdet3f(rbg);
		rbgIn[2][2] = mdet2f(Im22)/mdet3f(rbg);
	}

	//라인95에서 처음에 Ncim에 cim을 넣어 줬으니까 축소하면 배경에 cim이 겹쳐 나오므로 Ncim을 검정으로 초기화해준다.
	for(int i=0; i<MAX_X; i++) {
		for(int j=0; j<MAX_Y; j++) {
			Ncim[i+MAX_X*j][0]=0;
			Ncim[i+MAX_X*j][1]=0;
			Ncim[i+MAX_X*j][2]=0;
		}
	}

	for(int i=0; i<MAX_X; i++) {
		for(int j=0; j<MAX_Y; j++) {
				vector(Ov,(float)i,(float)j,1.0); 
				mvmult3f (Iv, rbgIn, Ov);
				if((Iv[0]<MAX_X && Iv[0]>0) && (Iv[1]<MAX_Y && Iv[1]>0)) {
					Ncim[i+MAX_X*j][0] = cim[(int)Iv[0]+MAX_X*((int)Iv[1])][0];  
					Ncim[i+MAX_X*j][1] = cim[(int)Iv[0]+MAX_X*((int)Iv[1])][1];  
					Ncim[i+MAX_X*j][2] = cim[(int)Iv[0]+MAX_X*((int)Iv[1])][2];  
				}
		}
	}
}

void Image::shearing ()
{
	matrix3f rbg; 
	V3DF Ov;
	V3DF Iv;
	int SPx,SPy;
	V3DF VP1;
	V3DF VP2;
	V3DF VP3;
	V3DF VP4;

	matrix (rbg, 1.0, 0.6, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0);
	
	float det;
	matrix3f rbgIn;
	matrix (rbgIn, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

	det = mdet3f(rbg);
	if(det==0) {
		printf("역행렬이 없습니다. \n");
	    return;
	}
	else {
		//역행렬만들기 위한 2차원 행렬 선언부분
		matrix2f Im00 = {rbg[1][1],rbg[1][2],rbg[2][1],rbg[2][2]};
		matrix2f Im01 = {rbg[1][0],rbg[1][2],rbg[2][0],rbg[2][2]};
		matrix2f Im02 = {rbg[1][0],rbg[1][1],rbg[2][0],rbg[2][1]};
		matrix2f Im10 = {rbg[0][1],rbg[0][2],rbg[2][1],rbg[2][2]};
		matrix2f Im11 = {rbg[0][0],rbg[0][2],rbg[2][0],rbg[2][2]};
		matrix2f Im12 = {rbg[0][0],rbg[0][1],rbg[2][0],rbg[2][1]};
		matrix2f Im20 = {rbg[0][1],rbg[0][2],rbg[1][1],rbg[1][2]};
		matrix2f Im21 = {rbg[0][0],rbg[0][2],rbg[1][0],rbg[1][2]};
		matrix2f Im22 = {rbg[0][0],rbg[0][1],rbg[1][0],rbg[1][1]};

		rbgIn[0][0] = mdet2f(Im00)/mdet3f(rbg);
		rbgIn[0][1] = -mdet2f(Im10)/mdet3f(rbg);
		rbgIn[0][2] = mdet2f(Im20)/mdet3f(rbg);
		rbgIn[1][0] = -mdet2f(Im01)/mdet3f(rbg);
		rbgIn[1][1] = mdet2f(Im11)/mdet3f(rbg);
		rbgIn[1][2] = -mdet2f(Im21)/mdet3f(rbg);
		rbgIn[2][0] = mdet2f(Im02)/mdet3f(rbg);
		rbgIn[2][1] = -mdet2f(Im12)/mdet3f(rbg);
		rbgIn[2][2] = mdet2f(Im22)/mdet3f(rbg);
	}

	//라인95에서 처음에 Ncim에 cim을 넣어 줬으니까 축소하면 배경에 cim이 겹쳐 나오므로 Ncim을 검정으로 초기화해준다.
	for(int i=0; i<MAX_X; i++) {
		for(int j=0; j<MAX_Y; j++) {
			Ncim[i+MAX_X*j][0]=0;
			Ncim[i+MAX_X*j][1]=0;
			Ncim[i+MAX_X*j][2]=0;
		}
	}

	for(int i=0; i<MAX_X; i++) {
		for(int j=0; j<MAX_Y; j++) {
			if(i==145 && j==599)
				printf("하하");
				vector(Ov,(float)i,(float)j,1.0); 
				mvmult3f (Iv, rbgIn, Ov);
				SPx = (int)floor(Iv[0]);
				SPy = (int)floor(Iv[1]);
				if(((SPx>0) && (SPx<MAX_X)) && ((SPy>0) && (SPy<MAX_Y)) && ((SPx) + MAX_X*(SPy+1)<300000)) {
					vertexV(VP1, VP2, VP3, VP4, SPx, SPy); 
					Ncim[i+MAX_X*j][0] = (float)(cim[(SPx) + MAX_X*(SPy)][0] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][0] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][0] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][0] * Extent(Iv, VP1));
					Ncim[i+MAX_X*j][1] = (float)(cim[(SPx) + MAX_X*(SPy)][1] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][1] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][1] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][1] * Extent(Iv, VP1));
					Ncim[i+MAX_X*j][2] = (float)(cim[(SPx) + MAX_X*(SPy)][2] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][2] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][2] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][2] * Extent(Iv, VP1));
				}
				else {
					Ncim[i+MAX_X*j][0] = (float)0;
					Ncim[i+MAX_X*j][1] = (float)0;
					Ncim[i+MAX_X*j][2] = (float)0;
				}
		}
	}
}

int t=100;
void Image::textrd(char *fn)
{
	FILE * fp = fopen(fn, "rt");

	if(fp == NULL)
	{
		printf("파일을 열수 없습니다\n");
		exit (0);
	}
	sctem = (float *)malloc(640000*sizeof(float));

	for (int k=0; k<640000; k++)
		fscanf(fp , "%f" , &sctem[k]);	

	for(int j=0; j<canvas.MAX_X*canvas.MAX_Y; j+=30500) {
		brush.rotation(sctem,index,brush.MAX_X,brush.MAX_Y);
		
		index+=30500;  //index+=31700;

		//j는 돌리는 횟수랑 관련=> 숫자가 작을수록 MAX_X*canvas.MAX_Y 범위안에서 더 많이도니까! 더 조밀하게찍힘. 
		//index값: 뒤에서 pushTexture2 함수호출되었을떄 이 index값을 가지고 캔버스 NCIM의 x, y좌표값을 계산하고 그 위치에 스트로크 찍음 > index값이 MAX_X로 나눴을때 딱맞아떨어지면 Y값에 변화가 없으므로
		//->index가 MAX_X값으로 딱 안나눠떨어지게 해야 Y좌표값이 계속 변해서 모양이 퍼지게 나옴!
		//j 랑 index값 숫자 조절하다보면 에러뜨는 숫자있는데 원인 모르게씀..ㅜㅜ소숫점땜에 긍가? 얼추 계산하면서 걍 적절하게 조절해서 잘나오는숫자 골랐엉ㅋㅋㅋㅋ
	}
		
		fclose ( fp );
}
void Image::translation ()
{
	matrix3f rbg; 
	V3DF Ov;
	V3DF Iv;
	int SPx,SPy;
	V3DF VP1;
	V3DF VP2;
	V3DF VP3;
	V3DF VP4;

	matrix (rbg, 1.0, 0.0, 50.0, 
		         0.0, 1.0, 30.0, 
				 0.0, 0.0, 1.0);
	
	float det;
	matrix3f rbgIn;
	matrix (rbgIn, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

	det = mdet3f(rbg);
	if(det==0) {
		printf("역행렬이 없습니다. \n");
	    return;
	}
	else {
		//역행렬만들기 위한 2차원 행렬 선언부분
		matrix2f Im00 = {rbg[1][1],rbg[1][2],rbg[2][1],rbg[2][2]};
		matrix2f Im01 = {rbg[1][0],rbg[1][2],rbg[2][0],rbg[2][2]};
		matrix2f Im02 = {rbg[1][0],rbg[1][1],rbg[2][0],rbg[2][1]};
		matrix2f Im10 = {rbg[0][1],rbg[0][2],rbg[2][1],rbg[2][2]};
		matrix2f Im11 = {rbg[0][0],rbg[0][2],rbg[2][0],rbg[2][2]};
		matrix2f Im12 = {rbg[0][0],rbg[0][1],rbg[2][0],rbg[2][1]};
		matrix2f Im20 = {rbg[0][1],rbg[0][2],rbg[1][1],rbg[1][2]};
		matrix2f Im21 = {rbg[0][0],rbg[0][2],rbg[1][0],rbg[1][2]};
		matrix2f Im22 = {rbg[0][0],rbg[0][1],rbg[1][0],rbg[1][1]};

		rbgIn[0][0] = mdet2f(Im00)/mdet3f(rbg);
		rbgIn[0][1] = -mdet2f(Im10)/mdet3f(rbg);
		rbgIn[0][2] = mdet2f(Im20)/mdet3f(rbg);
		rbgIn[1][0] = -mdet2f(Im01)/mdet3f(rbg);
		rbgIn[1][1] = mdet2f(Im11)/mdet3f(rbg);
		rbgIn[1][2] = -mdet2f(Im21)/mdet3f(rbg);
		rbgIn[2][0] = mdet2f(Im02)/mdet3f(rbg);
		rbgIn[2][1] = -mdet2f(Im12)/mdet3f(rbg);
		rbgIn[2][2] = mdet2f(Im22)/mdet3f(rbg);
	}

	//라인95에서 처음에 Ncim에 cim을 넣어 줬으니까 축소하면 배경에 cim이 겹쳐 나오므로 Ncim을 검정으로 초기화해준다.
	for(int i=0; i<MAX_X; i++) {
		for(int j=0; j<MAX_Y; j++) {
			Ncim[i+MAX_X*j][0]=0;
			Ncim[i+MAX_X*j][1]=0;
			Ncim[i+MAX_X*j][2]=0;
		}
	}

	for(int i=0; i<MAX_X; i++) {
		for(int j=0; j<MAX_Y; j++) {
				vector(Ov,(float)i,(float)j,1.0); 
				mvmult3f (Iv, rbgIn, Ov);
				SPx = (int)floor(Iv[0]);
				SPy = (int)floor(Iv[1]);
				if(((SPx>0) && (SPx<MAX_X)) && ((SPy>0) && (SPy<MAX_Y)) && ((SPx) + MAX_X*(SPy+1)<300000)) {
					vertexV(VP1, VP2, VP3, VP4, SPx, SPy); 
					Ncim[i+MAX_X*j][0] = (float)(cim[(SPx) + MAX_X*(SPy)][0] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][0] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][0] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][0] * Extent(Iv, VP1));
					Ncim[i+MAX_X*j][1] = (float)(cim[(SPx) + MAX_X*(SPy)][1] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][1] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][1] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][1] * Extent(Iv, VP1));
					Ncim[i+MAX_X*j][2] = (float)(cim[(SPx) + MAX_X*(SPy)][2] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][2] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][2] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][2] * Extent(Iv, VP1));
				}
				else {
					Ncim[i+MAX_X*j][0] = (float)0;
					Ncim[i+MAX_X*j][1] = (float)0;
					Ncim[i+MAX_X*j][2] = (float)0;
				}
		}
	}
}

void Image::compositionSR ()
{
	matrix3f rbgR;
	matrix3f rbgS;
	matrix3f rbgSR;
	V3DF Ov;
	V3DF Iv;
	int SPx,SPy;
	V3DF VP1;
	V3DF VP2;
	V3DF VP3;
	V3DF VP4;

	matrix (rbgR, cos(3.1415926/180*30), -sin(3.1415926/180*30), 0.0, sin(3.1415926/180*30), cos(3.1415926/180*30), 0.0, 0.0, 0.0, 1.0);
	matrix (rbgS, 0.5, 0.0, 0.0, 0.0, 0.3, 0.0, 0.0, 0.0, 1.0);
	mmult3f (rbgSR, rbgR, rbgS);
	
	float det;
	matrix3f rbgIn;
	matrix (rbgIn, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

	det = mdet3f(rbgSR);
	if(det==0) {
		printf("역행렬이 없습니다. \n");
	    return;
	}
	else {
		//역행렬만들기 위한 2차원 행렬 선언부분
		matrix2f Im00 = {rbgSR[1][1],rbgSR[1][2],rbgSR[2][1],rbgSR[2][2]};
		matrix2f Im01 = {rbgSR[1][0],rbgSR[1][2],rbgSR[2][0],rbgSR[2][2]};
		matrix2f Im02 = {rbgSR[1][0],rbgSR[1][1],rbgSR[2][0],rbgSR[2][1]};
		matrix2f Im10 = {rbgSR[0][1],rbgSR[0][2],rbgSR[2][1],rbgSR[2][2]};
		matrix2f Im11 = {rbgSR[0][0],rbgSR[0][2],rbgSR[2][0],rbgSR[2][2]};
		matrix2f Im12 = {rbgSR[0][0],rbgSR[0][1],rbgSR[2][0],rbgSR[2][1]};
		matrix2f Im20 = {rbgSR[0][1],rbgSR[0][2],rbgSR[1][1],rbgSR[1][2]};
		matrix2f Im21 = {rbgSR[0][0],rbgSR[0][2],rbgSR[1][0],rbgSR[1][2]};
		matrix2f Im22 = {rbgSR[0][0],rbgSR[0][1],rbgSR[1][0],rbgSR[1][1]};

		rbgIn[0][0] = mdet2f(Im00)/mdet3f(rbgSR);
		rbgIn[0][1] = -mdet2f(Im10)/mdet3f(rbgSR);
		rbgIn[0][2] = mdet2f(Im20)/mdet3f(rbgSR);
		rbgIn[1][0] = -mdet2f(Im01)/mdet3f(rbgSR);
		rbgIn[1][1] = mdet2f(Im11)/mdet3f(rbgSR);
		rbgIn[1][2] = -mdet2f(Im21)/mdet3f(rbgSR);
		rbgIn[2][0] = mdet2f(Im02)/mdet3f(rbgSR);
		rbgIn[2][1] = -mdet2f(Im12)/mdet3f(rbgSR);
		rbgIn[2][2] = mdet2f(Im22)/mdet3f(rbgSR);
	}

	//라인95에서 처음에 Ncim에 cim을 넣어 줬으니까 축소하면 배경에 cim이 겹쳐 나오므로 Ncim을 검정으로 초기화해준다.
	for(int i=0; i<MAX_X; i++) {
		for(int j=0; j<MAX_Y; j++) {
			Ncim[i+MAX_X*j][0]=0;
			Ncim[i+MAX_X*j][1]=0;
			Ncim[i+MAX_X*j][2]=0;
		}
	}

	for(int i=0; i<MAX_X; i++) {
		for(int j=0; j<MAX_Y; j++) {
				vector(Ov,(float)i,(float)j,1.0); 
				mvmult3f (Iv, rbgIn, Ov);
				SPx = (int)floor(Iv[0]);
				SPy = (int)floor(Iv[1]);
				if(((SPx>0) && (SPx<MAX_X)) && ((SPy>0) && (SPy<MAX_Y)) && ((SPx) + MAX_X*(SPy+1)<300000)) {
					vertexV(VP1, VP2, VP3, VP4, SPx, SPy); 
					Ncim[i+MAX_X*j][0] = (float)(cim[(SPx) + MAX_X*(SPy)][0] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][0] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][0] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][0] * Extent(Iv, VP1));
					Ncim[i+MAX_X*j][1] = (float)(cim[(SPx) + MAX_X*(SPy)][1] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][1] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][1] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][1] * Extent(Iv, VP1));
					Ncim[i+MAX_X*j][2] = (float)(cim[(SPx) + MAX_X*(SPy)][2] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][2] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][2] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][2] * Extent(Iv, VP1));
				}
				else {
					Ncim[i+MAX_X*j][0] = (float)0;
					Ncim[i+MAX_X*j][1] = (float)0;
					Ncim[i+MAX_X*j][2] = (float)0;
				}
		}
	}
}

void Image::compositionRS ()
{
	matrix3f rbgR;
	matrix3f rbgS;
	matrix3f rbgSR;
	V3DF Ov;
	V3DF Iv;
	int SPx,SPy;
	V3DF VP1;
	V3DF VP2;
	V3DF VP3;
	V3DF VP4;

	matrix (rbgR, cos(3.1415926/180*30), -sin(3.1415926/180*30), 0.0, sin(3.1415926/180*30), cos(3.1415926/180*30), 0.0, 0.0, 0.0, 1.0);
	matrix (rbgS, 0.5, 0.0, 0.0, 0.0, 0.3, 0.0, 0.0, 0.0, 1.0);
	mmult3f (rbgSR, rbgS, rbgR);
	
	float det;
	matrix3f rbgIn;
	matrix (rbgIn, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

	det = mdet3f(rbgSR);
	if(det==0) {
		printf("역행렬이 없습니다. \n");
	    return;
	}
	else {
		//역행렬만들기 위한 2차원 행렬 선언부분
		matrix2f Im00 = {rbgSR[1][1],rbgSR[1][2],rbgSR[2][1],rbgSR[2][2]};
		matrix2f Im01 = {rbgSR[1][0],rbgSR[1][2],rbgSR[2][0],rbgSR[2][2]};
		matrix2f Im02 = {rbgSR[1][0],rbgSR[1][1],rbgSR[2][0],rbgSR[2][1]};
		matrix2f Im10 = {rbgSR[0][1],rbgSR[0][2],rbgSR[2][1],rbgSR[2][2]};
		matrix2f Im11 = {rbgSR[0][0],rbgSR[0][2],rbgSR[2][0],rbgSR[2][2]};
		matrix2f Im12 = {rbgSR[0][0],rbgSR[0][1],rbgSR[2][0],rbgSR[2][1]};
		matrix2f Im20 = {rbgSR[0][1],rbgSR[0][2],rbgSR[1][1],rbgSR[1][2]};
		matrix2f Im21 = {rbgSR[0][0],rbgSR[0][2],rbgSR[1][0],rbgSR[1][2]};
		matrix2f Im22 = {rbgSR[0][0],rbgSR[0][1],rbgSR[1][0],rbgSR[1][1]};

		rbgIn[0][0] = mdet2f(Im00)/mdet3f(rbgSR);
		rbgIn[0][1] = -mdet2f(Im10)/mdet3f(rbgSR);
		rbgIn[0][2] = mdet2f(Im20)/mdet3f(rbgSR);
		rbgIn[1][0] = -mdet2f(Im01)/mdet3f(rbgSR);
		rbgIn[1][1] = mdet2f(Im11)/mdet3f(rbgSR);
		rbgIn[1][2] = -mdet2f(Im21)/mdet3f(rbgSR);
		rbgIn[2][0] = mdet2f(Im02)/mdet3f(rbgSR);
		rbgIn[2][1] = -mdet2f(Im12)/mdet3f(rbgSR);
		rbgIn[2][2] = mdet2f(Im22)/mdet3f(rbgSR);
	}

	//라인95에서 처음에 Ncim에 cim을 넣어 줬으니까 축소하면 배경에 cim이 겹쳐 나오므로 Ncim을 검정으로 초기화해준다.
	for(int i=0; i<MAX_X; i++) {
		for(int j=0; j<MAX_Y; j++) {
			Ncim[i+MAX_X*j][0]=0;
			Ncim[i+MAX_X*j][1]=0;
			Ncim[i+MAX_X*j][2]=0;
		}
	}

	for(int i=0; i<MAX_X; i++) {
		for(int j=0; j<MAX_Y; j++) {
				vector(Ov,(float)i,(float)j,1.0); 
				mvmult3f (Iv, rbgIn, Ov);
				SPx = (int)floor(Iv[0]);
				SPy = (int)floor(Iv[1]);
				if(((SPx>0) && (SPx<MAX_X)) && ((SPy>0) && (SPy<MAX_Y)) && ((SPx) + MAX_X*(SPy+1)<300000)) {
					vertexV(VP1, VP2, VP3, VP4, SPx, SPy); 
					Ncim[i+MAX_X*j][0] = (float)(cim[(SPx) + MAX_X*(SPy)][0] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][0] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][0] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][0] * Extent(Iv, VP1));
					Ncim[i+MAX_X*j][1] = (float)(cim[(SPx) + MAX_X*(SPy)][1] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][1] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][1] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][1] * Extent(Iv, VP1));
					Ncim[i+MAX_X*j][2] = (float)(cim[(SPx) + MAX_X*(SPy)][2] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][2] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][2] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][2] * Extent(Iv, VP1));
				}
				else {
					Ncim[i+MAX_X*j][0] = (float)0;
					Ncim[i+MAX_X*j][1] = (float)0;
					Ncim[i+MAX_X*j][2] = (float)0;
				}
		}
	}
}

void Image::sliding (float SV)
{
	if(SV>=0) {
		for(int i=0; i<MAX_X; i++) {
			for(int j=0; j<MAX_Y; j++){
					//printf("(%d,%d) 일 때, rgb 값 : %f \n", i,j, cim[i+MAX_X*j][0]);
				if(cim[i+MAX_X*j][0]+SV >= 1.0) {
					Ncim[i+MAX_X*j][0]=1.0;
					Ncim[i+MAX_X*j][1]=1.0;
					Ncim[i+MAX_X*j][2]=1.0;
					//printf(" 슬라이딩한 후(%d,%d) 일 때, rgb 값 : %f \n", i,j, cim[i+MAX_X*j][0]);
				}
				else { 
					Ncim[i+MAX_X*j][0] = cim[i+MAX_X*j][0]+SV;
					Ncim[i+MAX_X*j][1] = cim[i+MAX_X*j][1]+SV;
					Ncim[i+MAX_X*j][2] = cim[i+MAX_X*j][2]+SV; 
					//printf(" 슬라이딩한 후(%d,%d) 일 때, cim rgb 값 : %f , Ncim rgb 값 :  %f \n", i,j, cim[i+MAX_X*j][0], Ncim[i+MAX_X*j][0]);
				}
			}
		}
	}
	else {
		for(int i=0; i<MAX_X; i++) {
			for(int j=0; j<MAX_Y; j++){
					//printf("(%d,%d) 일 때, rgb 값 : %f \n", i,j, cim[i+MAX_X*j][0]);
				if(cim[i+MAX_X*j][0]+SV <= 0.0) {
					Ncim[i+MAX_X*j][0]=0.0;
					Ncim[i+MAX_X*j][1]=0.0;
					Ncim[i+MAX_X*j][2]=0.0;
					//printf(" 슬라이딩한 후(%d,%d) 일 때, rgb 값 : %f \n", i,j, cim[i+MAX_X*j][0]);
				}
				else { 
					Ncim[i+MAX_X*j][0] = cim[i+MAX_X*j][0]+SV;
					Ncim[i+MAX_X*j][1] = cim[i+MAX_X*j][1]+SV;
					Ncim[i+MAX_X*j][2] = cim[i+MAX_X*j][2]+SV; 
					//printf(" 슬라이딩한 후(%d,%d) 일 때, cim rgb 값 : %f , Ncim rgb 값 :  %f \n", i,j, cim[i+MAX_X*j][0], Ncim[i+MAX_X*j][0]);
				}
			}
		}
	}
}

void Image::stretching() 
{
	float LV,HV;
	LV = cim[0][0];
	HV = cim[0][0];

	for(int i=0; i<MAX_X; i++) {
		for(int j=0; j<MAX_Y; j++) {
			if(cim[i+MAX_X*j][0]<LV)
				LV = cim[i+MAX_X*j][0];
			else
				continue;
		} 
	}
	printf(" 가장 어두운 값은 ? %f \n", LV);

	for(int i=0; i<MAX_X; i++) {
		for(int j=0; j<MAX_Y; j++) {
			if(cim[i+MAX_X*j][0]>HV)
				HV = cim[i+MAX_X*j][0];
			else
				continue;
		} 
	}
	printf(" 가장 밝 값은 ? %f \n", HV);
	sliding(-0.4);
	//float LV1,HV1;
	//LV1 = Ncim[0][0];
	//HV1 = Ncim[0][0];
	//for(int i=0; i<MAX_X; i++) {
	//	for(int j=0; j<MAX_Y; j++) {
	//		if(Ncim[i+MAX_X*j][0]<LV1)
	//			LV1 = Ncim[i+MAX_X*j][0];
	//		else
	//			continue;
	//	} 
	//}
	//printf(" 가장 어두운 값은 ? %f \n", LV1);

	//for(int i=0; i<MAX_X; i++) {
	//	for(int j=0; j<MAX_Y; j++) {
	//		if(Ncim[i+MAX_X*j][0]>HV1)
	//			HV1 = Ncim[i+MAX_X*j][0];
	//		else
	//			continue;
	//	} 
	//}
	//printf(" 가장 밝 값은 ? %f\n", HV1);
	sliding(LV);

	for(int i=0; i<MAX_X; i++) {
		for(int j=0; j<MAX_Y; j++) {
			Ncim[i+MAX_X*j][0] = (cim[i+MAX_X*j][0]-LV)*(1/(HV-LV));
			Ncim[i+MAX_X*j][1] = (cim[i+MAX_X*j][0]-LV)*(1/(HV-LV));
			Ncim[i+MAX_X*j][2] = (cim[i+MAX_X*j][0]-LV)*(1/(HV-LV));
		}
	}
}

void Image::equalizationBW()
{
	int cntc[256]; //밝기 별 픽셀수
	float pr[256]; // 밝기 별 확률
	float prA[256]; // 밝기 별 확률 누적 
	int Tcntc[256];
	float RTcntc[256];// 새로 변환 된 밝기
	int pl; 
	int result;
	float temp;
	float temp2;
	float temp3;
	 
	// cntc 초기화하기
	for(int i=0; i<256; i++) {
		cntc[i]=0;
		Tcntc[i]=0;
		prA[i]=0;
	}

	// 밝기 별 빈도수를 구하는 과정. 255를 곱하고 반올림 까지 한 값을 최종적으로 pl에 넣는다. - 흑백에 적용하는 것.
	for(int i=0; i<MAX_X; i++) {
		for(int j=0; j<MAX_Y; j++) {
			temp = cim[i+MAX_X*j][0]*255;
			pl = floor(temp + 0.5);
			cntc[pl]++;
		} 
	}
	for(int i=80; i<150; i++)
		printf("%d 밝기의 빈도수는 %d \n", i, cntc[i]);

	// 밝기 별 확률을 구하는 과정.
	for(int i=0; i<256; i++) 
		pr[i] = (float)cntc[i]/(float)(MAX_X*MAX_Y);
	printf("픽셀 수 %d \n", MAX_X*MAX_Y);
	printf("확률 %f %f %f\n", pr[115], pr[116], pr[117]);

	// 밝기 별 확률의 누적을 구하는 과정.
	prA[0]=pr[0];
	for(int i=1; i<256; i++)
		prA[i] = prA[i-1] + pr[i];
	printf("누적 확률 %f %f %f", prA[115], prA[116], prA[117]);
	// 새로 변환 된 밝기 구하기.
	for(int i=0; i<256; i++) {
		temp2 = prA[i] * 255;
		Tcntc[i] = floor(temp2 + 0.5);
	}

	// 변환 된 색깔 적용하기
	for(int i=0; i<MAX_X; i++) {
		for(int j=0; j<MAX_Y; j++) {
			temp3 = cim[i+MAX_X*j][0]*255;
			result = floor(temp3 + 0.5);
			RTcntc[result] = Tcntc[result]/255;
			Ncim[i+MAX_X*j][0] = RTcntc[result];
			Ncim[i+MAX_X*j][1] = RTcntc[result];
			Ncim[i+MAX_X*j][2] = RTcntc[result];
		}
	}
}

void Image::equalizationCL()
{
	int cntc[256]; //밝기 별 픽셀수
	float pr[256]; // 밝기 별 확률
	float prA[256]; // 밝기 별 확률 누적 
	int Tcntc[256];
	float RTcntc[256];// 새로 변환 된 밝기
	int pl; 
	int result;
	float temp;
	float temp2;
	float temp3;
	 
	// cntc 초기화하기
	for(int i=0; i<256; i++) {
		cntc[i]=0;
		Tcntc[i]=0;
		prA[i]=0;
	}

	// 밝기 별 빈도수를 구하는 과정. 255를 곱하고 반올림 까지 한 값을 최종적으로 pl에 넣는다. - 흑백에 적용하는 것.
	for(int i=0; i<MAX_X; i++) {
		for(int j=0; j<MAX_Y; j++) {
			temp = (cim[i+MAX_X*j][0]+cim[i+MAX_X*j][1]+cim[i+MAX_X*j][2])/3*255;
			pl = floor(temp + 0.5);
			cntc[pl]++;
		} 
	}
	for(int i=80; i<150; i++)
		printf("%d 밝기의 빈도수는 %d \n", i, cntc[i]);

	// 밝기 별 확률을 구하는 과정.
	for(int i=0; i<256; i++) 
		pr[i] = (float)cntc[i]/(float)(MAX_X*MAX_Y);
	printf("픽셀 수 %d \n", MAX_X*MAX_Y);
	printf("확률 %f %f %f\n", pr[115], pr[116], pr[117]);

	// 밝기 별 확률의 누적을 구하는 과정.
	prA[0]=pr[0];
	for(int i=1; i<256; i++)
		prA[i] = prA[i-1] + pr[i];
	printf("누적 확률 %f %f %f", prA[115], prA[116], prA[117]);
	// 새로 변환 된 밝기 구하기.
	for(int i=0; i<256; i++) {
		temp2 = prA[i] * 255;
		Tcntc[i] = floor(temp2 + 0.5);
	}

	// 변환 된 색깔 적용하기
	for(int i=0; i<MAX_X; i++) {
		for(int j=0; j<MAX_Y; j++) {
			temp3 = (cim[i+MAX_X*j][0]+cim[i+MAX_X*j][1]+cim[i+MAX_X*j][2])/3*255;
			result = floor(temp3 + 0.5);
			RTcntc[result] = (float)Tcntc[result]/(float)255*(float)3;
			Ncim[i+MAX_X*j][0] = RTcntc[result]*cim[i+MAX_X*j][0]/(cim[i+MAX_X*j][0]+cim[i+MAX_X*j][1]+cim[i+MAX_X*j][2]);
			Ncim[i+MAX_X*j][1] = RTcntc[result]*cim[i+MAX_X*j][1]/(cim[i+MAX_X*j][0]+cim[i+MAX_X*j][1]+cim[i+MAX_X*j][2]);
			Ncim[i+MAX_X*j][2] = RTcntc[result]*cim[i+MAX_X*j][2]/(cim[i+MAX_X*j][0]+cim[i+MAX_X*j][1]+cim[i+MAX_X*j][2]);
		}
	}
	printf("변화된 밝기 %f %f %f \n",Ncim[500][0],Ncim[500][1],Ncim[500][2] );
}
void Image::compositionSR2( float r, int index, float width, float height, float s2 )
{
	V3DF Ov;
	V3DF Iv;
	int SPx,SPy;
	V3DF VP1;
	V3DF VP2;
	V3DF VP3;
	V3DF VP4;

				//R정의
				matrix3f rotate; 
				matrix (rotate, 
							cos(3.1415926/180*r), -sin(3.1415926/180*r), 0.0, 
							sin(3.1415926/180*r), cos(3.1415926/180*r), 0.0, 
							0.0, 0.0, 1.0 );
				//T정의
				matrix3f trans;
				matrix ( trans, 
							1.0, 0.0, -(width/2), 
							0.0, 1.0, -(height/2), 
							0.0, 0.0, 1.0 );
				//T의 Inverse 정의
				float det;
				matrix3f transIn;
				matrix (transIn, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

				det = mdet3f(trans);
				if(det==0) 
				{
					printf("역행렬이 없습니다. \n");
					return;
				}
				else 
				{
					//역행렬만들기 위한 2차원 행렬 선언부분
					matrix2f Im00 = {trans[1][1],trans[1][2],trans[2][1],trans[2][2]};
					matrix2f Im01 = {trans[1][0],trans[1][2],trans[2][0],trans[2][2]};
					matrix2f Im02 = {trans[1][0],trans[1][1],trans[2][0],trans[2][1]};
					matrix2f Im10 = {trans[0][1],trans[0][2],trans[2][1],trans[2][2]};
					matrix2f Im11 = {trans[0][0],trans[0][2],trans[2][0],trans[2][2]};
					matrix2f Im12 = {trans[0][0],trans[0][1],trans[2][0],trans[2][1]};
					matrix2f Im20 = {trans[0][1],trans[0][2],trans[1][1],trans[1][2]};
					matrix2f Im21 = {trans[0][0],trans[0][2],trans[1][0],trans[1][2]};
					matrix2f Im22 = {trans[0][0],trans[0][1],trans[1][0],trans[1][1]};

					transIn[0][0] = mdet2f(Im00)/mdet3f(trans);
					transIn[0][1] = -mdet2f(Im10)/mdet3f(trans);
					transIn[0][2] = mdet2f(Im20)/mdet3f(trans);
					transIn[1][0] = -mdet2f(Im01)/mdet3f(trans);
					transIn[1][1] = mdet2f(Im11)/mdet3f(trans);
					transIn[1][2] = -mdet2f(Im21)/mdet3f(trans);
					transIn[2][0] = mdet2f(Im02)/mdet3f(trans);
					transIn[2][1] = -mdet2f(Im12)/mdet3f(trans);
					transIn[2][2] = mdet2f(Im22)/mdet3f(trans);
				}
				//S정의
				matrix3f scale;
				matrix (scale, 
								s2, 0.0, 0.0, 
								0.0, s2, 0.0, 
								0.0, 0.0, 1.0);
				//result계산 ( T-1*R*S*T )
				matrix3f result;
				mmult3f (result, rotate, trans);
				mmult3f (result, scale, result);
				mmult3f (result, transIn, result);
	for(int i=0; i<MAX_X; i++)
				{
					for(int j=0; j<MAX_Y; j++) 
					{
						Ncim[i+MAX_X*j][0]=1;
						Ncim[i+MAX_X*j][1]=1;
						Ncim[i+MAX_X*j][2]=1;
					}
				}
				//=========================================result의 역행렬구하기->resultIn===========================
				float det2;
				matrix3f resultIn;
				matrix (resultIn, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

				det2 = mdet3f(result);
				if(det2==0) 
				{
					printf("역행렬이 없습니다. \n");
					return;
				}
				else 
				{
					//역행렬만들기 위한 2차원 행렬 선언부분
					matrix2f Im200 = {result[1][1],result[1][2],result[2][1],result[2][2]};
					matrix2f Im201 = {result[1][0],result[1][2],result[2][0],result[2][2]};
					matrix2f Im202 = {result[1][0],result[1][1],result[2][0],result[2][1]};
					matrix2f Im210 = {result[0][1],result[0][2],result[2][1],result[2][2]};
					matrix2f Im211 = {result[0][0],result[0][2],result[2][0],result[2][2]};
					matrix2f Im212 = {result[0][0],result[0][1],result[2][0],result[2][1]};
					matrix2f Im220 = {result[0][1],result[0][2],result[1][1],result[1][2]};
					matrix2f Im221 = {result[0][0],result[0][2],result[1][0],result[1][2]};
					matrix2f Im222 = {result[0][0],result[0][1],result[1][0],result[1][1]};

					resultIn[0][0] = mdet2f(Im200)/mdet3f(result);
					resultIn[0][1] = -mdet2f(Im210)/mdet3f(result);
					resultIn[0][2] = mdet2f(Im220)/mdet3f(result);
					resultIn[1][0] = -mdet2f(Im201)/mdet3f(result);
					resultIn[1][1] = mdet2f(Im211)/mdet3f(result);
					resultIn[1][2] = -mdet2f(Im221)/mdet3f(result);
					resultIn[2][0] = mdet2f(Im202)/mdet3f(result);
					resultIn[2][1] = -mdet2f(Im212)/mdet3f(result);
					resultIn[2][2] = mdet2f(Im222)/mdet3f(result);
				}

				//=========================================보간법을 이용해 벡터에적용시키기===========================

				for(int i=0; i<MAX_X; i++)
				{
					for(int j=0; j<MAX_Y; j++)
					{
							vector(Ov,(float)i,(float)j,1.0); 
							mvmult3f (Iv, resultIn, Ov);
							SPx = (int)floor(Iv[0]);
							SPy = (int)floor(Iv[1]);
							if(((SPx>0) && (SPx<MAX_X)) && ((SPy>0) && (SPy<MAX_Y)) && ((SPx) + MAX_X*(SPy+1)<(MAX_X*MAX_Y))) {
								vertexV(VP1, VP2, VP3, VP4, SPx, SPy); 
								Ncim[i+MAX_X*j][0] = (float)(cim[(SPx) + MAX_X*(SPy)][0] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][0] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][0] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][0] * Extent(Iv, VP1));
								Ncim[i+MAX_X*j][1] = (float)(cim[(SPx) + MAX_X*(SPy)][1] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][1] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][1] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][1] * Extent(Iv, VP1));
								Ncim[i+MAX_X*j][2] = (float)(cim[(SPx) + MAX_X*(SPy)][2] * Extent(Iv, VP4) + cim[(SPx) + MAX_X*(SPy+1)][2] * Extent(Iv, VP3) + cim[(SPx+1) + MAX_X*SPy][2] * Extent(Iv, VP2) + cim[(SPx+1) + MAX_X*(SPy+1)][2] * Extent(Iv, VP1));
							}
							else 
							{
								Ncim[i+MAX_X*j][0] = (float)1;
								Ncim[i+MAX_X*j][1] = (float)1;
								Ncim[i+MAX_X*j][2] = (float)1;
							}
					}
				}



}