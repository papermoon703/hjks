//	Image.h


//----------------------
#ifndef _IMAGE_H_
#define _IMAGE_H_
//----------------------

#include "main.h"
#include "BITMAP2.h"

#define RED_WEIGHT 0.299
#define GREEN_WEIGHT 0.587
#define BLUE_WEIGHT 0.114

class Image {
public:	
	V3DF *cim;	//color image(origianl image)
	float *im;	//gray image
	V3DF *Ncim; //변형시킨 이미지를 저장하는 것.
//	float theta[1024];
	void textrd(char *fn);
	float *sctem;
	int MAX_X, MAX_Y;
	void rotation(float *r,int index, float width, float height );
	void rotation2 (float r, float width, float height );
	void scaling( );
	void scaling2( float s , float width, float height );
    void shearing();
	void translation();
	void compositionSR();
	void compositionSR2( float r, int index, float width, float height , float s2 );
	void compositionRS();
	void sliding(float SV);
	void stretching();
	void equalizationBW();
	void equalizationCL();
	Image();
	~Image();

	bool loadBitmap2 ( char *fn );

	void draw ( );

};

//------------------
#endif
//------------------
