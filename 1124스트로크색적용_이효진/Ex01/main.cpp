
#include "main.h"
#include "BITMAP2.h"
#include "Image.h"
#include "vm.h"

int Width;
int Height;


Image canvas;
Image brush;
int num2;
int a=100;
int b=0;
char input[20] = "omaju01.bmp";
char input2[20] = "bbx_stroke_200.bmp";
char input3[20] = "omaju01.txt";
char input_Germany[20] = "Germany.bmp";
char input_France[20] = "France.bmp";
char input_Rwanda[20] = "Rwanda.bmp";

//	Global data structure for rendering
V3DF White = 		{ 1.0f, 1.0f, 1.0f };
V3DF LightGray =	{ 0.8f, 0.8f, 0.8f };
V3DF Gray = 		{ 0.5f, 0.5f, 0.5f };
V3DF DarkGray =		{ 0.2f, 0.2f, 0.2f };
V3DF Black =		{ 0.0f, 0.0f, 0.0f };
V3DF Red = 			{ 1.0f, 0.0f, 0.0f };
V3DF Green =		{ 0.0f, 1.0f, 0.0f };
V3DF Blue = 		{ 0.0f, 0.0f, 1.0f };
V3DF Cyan =		 	{ 0.0f, 1.0f, 1.0f };
V3DF Violet = 		{ 1.0f, 0.0f, 1.0f };
V3DF Yellow =		{ 1.0f, 1.0f, 0.0f };
V3DF Orange =		{ 1.0f, 0.5f, 0.0f };
V3DF LightRed = 	{ 0.8f, 0.0f, 0.0f };
V3DF LightGreen = 	{ 0.0f, 0.8f, 0.0f };
V3DF LightBlue = 	{ 0.0f, 0.0f, 0.8f };
V3DF LightCyan = 	{ 0.0f, 0.8f, 0.8f };
V3DF LightViolet = 	{ 0.8f, 0.0f, 0.8f };
V3DF LightYellow = 	{ 0.8f, 0.8f, 0.0f };
V3DF DarkRed = 		{ 0.3f, 0.0f, 0.0f };
V3DF DarkGreen = 	{ 0.0f, 0.3f, 0.0f };
V3DF DarkBlue = 	{ 0.0f, 0.0f, 0.3f };
V3DF DarkCyan = 	{ 0.0f, 0.3f, 0.3f };
V3DF DarkViolet = 	{ 0.3f, 0.0f, 0.3f };
V3DF DarkYellow = 	{ 0.3f, 0.3f, 0.0f };
V4DF TYellow =		{ 1.0f, 1.0f, 0.0f, 0.5f };
V3DF Whatisthis =	{ 1.0f, 0.0f, 0.5f };

int msec;
long sec;
struct _timeb tstruct;
time_t ltime;
FILE *fptime;

//	trackball.cpp: extern functions for camera control

extern void save_bitmap_file ( char *fn, int W, int H, GLubyte *rpxls );

extern void FLOATtoBMP2 ( char *fn, int Width, int Height, float *im );

extern void vector3f ( V3DF v, float v1, float v2, float v3 );
extern void vzero ( V3DF v );

V3DF veye, vcent, vup; 
V3DF cent;

int nv = 0;
Vertex vl[100000];
int STATE = 0;
int tPICTURE=0;	//0:x, 1:COLOR

void RGBtoXYZ(float R2, float G2, float B2, float *X2, float *Y2, float *Z2)
{
	
	float r = R2/255.0f;
	float g = G2/255.0f;
	float b = B2/255.0f;	//나눗셈 추가. 계산을 위한 임시 변수

	if ( r > 0.04045 ) r = pow( ((r+0.055f)/1.055f), 2.4f );
	else			   r = r/12.92f;

	if ( g > 0.04045 ) g = pow( ((g+0.055f)/1.055f), 2.4f );
	else			   g = g/12.92f;

	if ( b > 0.04045 ) b = pow( ((b+0.055f)/1.055f), 2.4f );
	else			   b = b/12.92f;

	r = r * 100;
	g = g * 100;
	b = b * 100;

	*X2 = (r*0.4124f) + (g*0.3576f) + (b*0.1805f);
	*Y2 = (r*0.2126f) + (g*0.7152f) + (b*0.0722f);
	*Z2 = (r*0.0193f) + (g*0.1192f) + (b*0.9505f);
}

void XYZtoLAB(float X2, float Y2, float Z2, float *L2, float *a2, float *b2)
{
	float x = 0.0f, y = 0.0f, z = 0.0f; // 초기화

	x = X2 / 95.047f;
	y = Y2 / 100.000f;
	z = Z2 / 108.883f;

	if ( x > 0.008856 ) x = pow( x, ( (float)1/3) );
	else				x = ( 7.787*x ) + ( (float)16/116 );
	if ( y > 0.008856 ) y = pow( y, ( (float)1/3) );
	else				y = ( 7.787*y ) + ( (float)16/116 );
	if ( z > 0.008856 ) z = pow( z, ( (float)1/3) );
	else				z = ( 7.787*z ) + ( (float)16/116 );		//오타 수정

	*L2 = ( 116.0f * y ) - 16.0f;
	*a2 = 500.0f * ( x - y );
	*b2 = 200.0f * ( y - z );
}

void LABtoXYZ( float L, float a, float b, float *X2, float *Y2, float *Z2 )
{
	float x = 0.0f, y = 0.0f, z = 0.0f; // 초기화

	y = ( L+16.0f )/116.0f;		//오타 수정 
	x = ( a/500.0f ) + y;
	z = y - ( b/200.0f ); 

	if ( pow( y, 3 ) > 0.008856 ) y = pow ( y, 3 );
	else					   y = ( y - ( 16.0f/116.0f) )/7.787f; 
	if ( pow( x, 3 ) > 0.008856 ) x = pow ( x, 3 );
	else					   x = ( x - ( 16.0f/116.0f) )/7.787f;
	if ( pow( z, 3 ) > 0.008856 ) z = pow ( z, 3 );
	else					   z = ( z - ( 16.0f/116.0f) )/7.787f;

	*X2 = 95.047f*x;
	*Y2 = 100.000f*y;
	*Z2 = 108.883f*z;
}

void XYZtoRGB(float X2, float Y2, float Z2, float *R2, float *G2, float *B2)
{
	float x = 0.0f, y = 0.0f, z = 0.0f; // 초기화
	float r = 0.0f, g = 0.0f, b = 0.0f; 

	x = X2/100.0f; 
	y = Y2/100.0f;
	z = Z2/100.0f;

	r = ( x * 3.2406f ) + ( y * (-1.5372f) ) + ( z * (-0.4986f) ); 
	g = ( x * (-0.9689f) ) + ( y * 1.8758f ) + ( z * 0.0415f );
	b = ( x * 0.0557f ) + ( y * (-0.2040f) ) + ( z * 1.0570f );

	if ( r > 0.0031308 ) r = 1.055f*(pow ( r , (1.0f/2.4f) )) - 0.055f;
	else				  r = 12.92*r;
	if ( g > 0.0031308 ) g = 1.055f*(pow ( g , (1.0f/2.4f) )) - 0.055f;
	else				  g = 12.92*g;
	if ( b > 0.0031308 ) b = 1.055f*(pow ( b , (1.0f/2.4f) )) - 0.055f;
	else				  b = 12.92*b;

	
	*R2 = r*255.0f;
	*G2 = g*255.0f;
	*B2 = b*255.0f;
}


void pushTexture( int x, int y )
{
	int i, j;
	printf("r : %f, g : %f, b : %f\n", canvas.Ncim[x+canvas.MAX_X*y][0],canvas.Ncim[x+canvas.MAX_X*y][1],canvas.Ncim[x+canvas.MAX_X*y][2]);

	float R2 = canvas.Ncim[x+canvas.MAX_X*y][0]*255, G2 = canvas.Ncim[x+canvas.MAX_X*y][1]*255, B2 = canvas.Ncim[x+canvas.MAX_X*y][2]*255;
	float X2 = 0.0f, Y2 = 0.0f, Z2 = 0.0f;
	float L = 0.0f, a = 0.0f, b = 0.0f;

	RGBtoXYZ(R2,G2,B2,&X2,&Y2,&Z2);
	XYZtoLAB(X2,Y2,Z2,&L,&a,&b); // 여기서 a,b값만 가져오기

	for ( int v = 0 ; v < brush.MAX_Y ; v++ ){
		for ( int u = 0 ; u < brush.MAX_X ; u++ ){
			i = u + ( x - ( brush.MAX_X/2 ) ); 
			j = v + ( y - ( brush.MAX_Y/2 ) );
			if ( j >= canvas.MAX_Y || j <= 0 || i <= 0 || i >= canvas.MAX_X )
				continue;
			if ( brush.Ncim[u+brush.MAX_X*v][0] < 0.91 && brush.Ncim[u+brush.MAX_X*v][1] < 0.91 && brush.Ncim[u+brush.MAX_X*v][2] < 0.91 ){
				// brush의 픽셀하나하나의 L 가져오기
				float R3 = brush.Ncim[u+brush.MAX_X*v][0]*255, G3 = brush.Ncim[u+brush.MAX_X*v][1]*255, B3 = brush.Ncim[u+brush.MAX_X*v][2]*255;
				float X3 = 0.0f, Y3 = 0.0f, Z3 = 0.0f;
				float L3 = 0.0f, a3 = 0.0f, b3 = 0.0f;
				// RGB -> L*a*b*
				RGBtoXYZ(R3,G3,B3,&X3,&Y3,&Z3);
				XYZtoLAB(X3,Y3,Z3,&L3,&a3,&b3);

				// 이제 Lab 합쳐서 다시 RGB로 변환하기
				float L4 = L3, a4 = a, b4 = b;
				float X4 = 0.0f, Y4 = 0.0f, Z4 = 0.0f;
				float R4 = 0.0f, G4 = 0.0f, B4 = 0.0f;

				LABtoXYZ(L4,a4,b4,&X4,&Y4,&Z4);
				XYZtoRGB(X4,Y4,Z4,&R4,&G4,&B4);

				canvas.Ncim[i+canvas.MAX_X*j][0] = (float)R4/255.0f;
				canvas.Ncim[i+canvas.MAX_X*j][1] = (float)G4/255.0f;
				canvas.Ncim[i+canvas.MAX_X*j][2] = (float)B4/255.0f;
			}
		}
	}
}

void pushTexture2 (int index)
{
	int i,j;
	for ( int u = 0 ; u < brush.MAX_X ; u++ ){
		for ( int v = 0 ; v < brush.MAX_Y ; v++){
			//인덱스값을 바로바로 매번 받아서 그 인덱스위치인 (x,y) 좌표를 중심으로 스트로크를 찍어야 하니까 i, j 값을 아래처럼 계산
			i = u+( (index/canvas.MAX_X) - ( brush.MAX_X/2 ) ); 
			j = v+(  (index%canvas.MAX_X) - ( brush.MAX_Y/2 ) );

			if ( j >= canvas.MAX_Y || j <= 0 || i <= 0 || i >= canvas.MAX_X )
				continue;
			if ( brush.Ncim[u+brush.MAX_X*v][0] < 0.9 && brush.Ncim[u+brush.MAX_X*v][1] < 0.9 && brush.Ncim[u+brush.MAX_X*v][2] < 0.9 ){
				canvas.Ncim[i+(canvas.MAX_X)*j][0] = brush.Ncim[u+brush.MAX_X*v][0];
				canvas.Ncim[i+(canvas.MAX_X)*j][1] = brush.Ncim[u+brush.MAX_X*v][1];
				canvas.Ncim[i+(canvas.MAX_X)*j][2] = brush.Ncim[u+brush.MAX_X*v][2];
			}
		}
	}

}

void addVertex ( int x, int y )
{
	V3DI pt2;

	pt2[0]=x;
	pt2[1]=Height-y;
	pt2[2]=0;

	vl[nv].pt2[0] = pt2[0];
	vl[nv].pt2[1] = pt2[1];
	vl[nv].pt2[2] = pt2[2];	 

	printf("%d, %d\n", pt2[0], pt2[1]);

	pushTexture( pt2[0], pt2[1] );
	nv++;
}

void hk_display ( void )
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity ( );

	glOrtho ( cent[X], Width + cent[X], cent[Y], Height + cent[Y], -1000.0f, 1000.0f );

	glClearColor ( 1.0f, 1.0f, 1.0f, 1.0f  );
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity ( );

	gluLookAt ( veye[0], veye[1], veye[2], vcent[0], vcent[1], vcent[2], vup[0], vup[1], vup[2] );


	canvas.draw ( );

	glutSwapBuffers ( );
}
void mouse (int button, int state, int x, int y)  
{
	switch ( button ){

		case GLUT_LEFT_BUTTON :
			switch ( state ){
				case GLUT_DOWN :
						addVertex(x,y);
					break;
				case GLUT_UP :
					break;
				}
			break;
		case GLUT_MIDDLE_BUTTON:
			break;
		case GLUT_RIGHT_BUTTON:
			break;
	}
   glutPostRedisplay();
}

void hk_keyboard ( unsigned char key, int x, int y )
{
	switch ( key ) {
		case 'r':
			//brush.rotation( 15.0f );
			brush.rotation2(15.0 , brush.MAX_X , brush.MAX_Y );
			//glutCreateWindow( "rotation" );
			//glutKeyboardFunc (hk_keyboard);
			//glutDisplayFunc(hk_display);
			break;
		case 's':
			canvas.textrd(input3);
//			brush.scaling();
//			glutCreateWindow( "scaling" );
//			glutKeyboardFunc (hk_keyboard);
			glutDisplayFunc(hk_display);
			break;
		//case 'i':
		////	brush.scaling2( 0.2 , brush.MAX_X, brush.MAX_Y );
		////	//thisImage.shearing();
		//	glutCreateWindow( "shearing" );
		//	glutKeyboardFunc(hk_keyboard);
		//	glutDisplayFunc(hk_display);
		//	break;
		case 't':
			canvas.loadBitmap2 ( input_Rwanda );
			glutCreateWindow( "Rwanda" );
			glutKeyboardFunc(hk_keyboard);
			glutMouseFunc (mouse);
			glutDisplayFunc(hk_display);
			break;
		case 'y':
			canvas.loadBitmap2 ( input_France );
			glutCreateWindow( "France" );
			glutKeyboardFunc(hk_keyboard);
			glutMouseFunc (mouse);
			glutDisplayFunc(hk_display);
			break;
//		case 'b':
//			brush.compositionRS();
//			glutCreateWindow( "compositionRS" );
//			glutKeyboardFunc(hk_keyboard);
//			glutDisplayFunc(hk_display);
//			break;
//		case 'c':  
//			thisImage.sliding(0.2);
//			glutCreateWindow( "sliding" );
//			glutKeyboardFunc(hk_keyboard);
//			glutDisplayFunc(hk_display);
//			break;
//		case 'd':  
//			thisImage.stretching();
//			glutCreateWindow( "stretching" );
//			glutKeyboardFunc(hk_keyboard);
//			glutDisplayFunc(hk_display);
//			break;
//		case 'e':
//			thisImage.equalizationBW();
//			glutCreateWindow( "equalizationBW" );
//			glutKeyboardFunc(hk_keyboard);
//			glutDisplayFunc(hk_display);
//			break;
//		case 'f':
//			thisImage.equalizationCL();
//			glutCreateWindow("equalizationCL");
//			glutKeyboardFunc(hk_keyboard);
//			glutDisplayFunc(hk_display);
//			break;
//		case 'q':
//		case 'Q':
//			exit ( 0 );
//			break;
//
		case 27:
			exit(0);
			break;
	}
	glutPostRedisplay ( );
}

void init(void)
{
	glClearColor ( 0.0f, 0.0f, 0.0f, 1.0f  );

	glViewport(0, 0, Width, Height);

	glEnable(GL_DEPTH_TEST);
	glClearDepth(1.0);

	glEnable ( GL_POLYGON_SMOOTH );
	glHint ( GL_POLYGON_SMOOTH_HINT, GL_NICEST );
	glHint ( GL_LINE_SMOOTH_HINT, GL_NICEST );
	

	glShadeModel(GL_SMOOTH);

	glDisable ( GL_BLEND );

	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);

	vector3f ( veye, 0.0f, 0.0f, 25.0f );
	vector3f ( vcent, 0.0f, 0.0f, 0.0f );
	vector3f ( vup, 0.0f, 1.0f, 0.0f );


	vzero ( cent );
} 

void main ( int argc, char *argv[] )
{
	brush.loadBitmap2 ( input2 );
	//canvas.loadBitmap2 ( input );  - 큰동그라미 omaju.bmp
	canvas.loadBitmap2 ( input_Germany );
	//setup();
	init();
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);

	glutInitWindowPosition(0,0);
	glutInitWindowSize(Width, Height);
	char wtitle[128];
	sprintf(wtitle, "Chances favor prepared minds: %s", input);
	glutCreateWindow( wtitle );
	
	
	glutKeyboardFunc (hk_keyboard);
	glutDisplayFunc(hk_display);
	glutMouseFunc (mouse);

	glutMainLoop();
}

