
#include <fstream>
#include "main.h"

////////////////////
//
//	vmg_math.cpp
//
///////////////////

void vzero ( V3DF v )
{
	v[0] = 0.0f;
	v[1] = 0.0f;
	v[2] = 0.0f;
}

void vector3i ( V3DI v, int v1, int v2, int v3 )
{
	v[0] = v1;
	v[1] = v2;
	v[2] = v3;
}

void vector3f ( V3DF v, float v1, float v2, float v3 )
{
	v[0] = v1;
	v[1] = v2;
	v[2] = v3;
}

//	vector outer product
void vcross ( V3DF vc, V3DF v1, V3DF v2 )
{
	vc[0] = v1[1]*v2[2] - v1[2]*v2[1];
	vc[1] = v1[2]*v2[0] - v1[0]*v2[2];
	vc[2] = v1[0]*v2[1] - v1[1]*v2[0];
}

void vassign ( V3DF dest, V3DF src )
{
	dest[0] = src[0];	dest[1] = src[1];	dest[2] = src[2];
}

float pdist2 ( V2DI pt1, V2DI pt2 )
{
	int i;
	float tmp;

	for ( i = 0, tmp = 0.0; i < 2; i++ )
		tmp += (pt1[i]-pt2[i])*(pt1[i]-pt2[i]);

	return (float) sqrt ( tmp );
}

//	vector inner product
float vdot ( V3DF v1, V3DF v2 )
{
	return v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2];
}

//	vector length square
float vlsquare ( V3DF v )
{
	return vdot ( v, v );
}

//	vector length
float vlength ( V3DF v )
{
	return sqrt ( vlsquare ( v ) );
}

//	vector add
//	dest = src1 + src2
void vadd ( V3DF dest, V3DF src1, V3DF src2 )
{
	dest[0] = src1[0] + src2[0];
	dest[1] = src1[1] + src2[1];
	dest[2] = src1[2] + src2[2];
}

//	vector normalize
bool vnorm ( V3DF v )
{
	float leng = vlength ( v );

	if ( leng == 0.0f )
		return 0.0f;

	v[0] /= leng;
	v[1] /= leng;
	v[2] /= leng;

	return 1.0f;

}

//	vector scalar multiplication
void vscalar ( V3DF v, float sca )
{
	v[0] *= sca;
	v[1] *= sca;
	v[2] *= sca;
}

//	vector add and assignment
//	dest = dest + src
void vsum ( V3DF dest, V3DF src ) 
{
	dest[0] += src[0];
	dest[1] += src[1];
	dest[2] += src[2];
}
