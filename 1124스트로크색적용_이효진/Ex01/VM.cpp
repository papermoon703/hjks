#include <stdio.h>
#include <stdlib.h>
#include "vm.h"


void vector (V3DF v1, float a, float b, float c) {
	v1[0] = a;
	v1[1] = b;
	v1[2] = c;
}

void vadd3f (V3DF a, V3DF b, V3DF c) {
	a[0] = b[0] + c[0];
	a[1] = b[1] + c[1];
	a[2] = b[2] + c[2];
}

void vcross3f (V3DF a, V3DF b, V3DF c) {
	a[0] = b[1]*c[2] - b[2]*c[1];
	a[1] = b[2]*c[0] - b[0]*c[2];
	a[2] = b[0]*c[1] - b[1]*c[0];
}

float vdot3f (V3DF a, V3DF b) {
	return a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
}

bool vequal3f (V3DF a, V3DF b) {
	if(a[0]==b[0] && a[1]==b[1] && a[2]==b[2])
		return true;
	else
		return false;
}

void vnorm3f (V3DF a) {
	a[0] = a[0]/sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
	a[1] = a[1]/sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
	a[2] = a[2]/sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
}

void matrix (matrix3f x, float a, float b, float c, float d, float e, float f, float g, float h, float i) {
	x[0][0] = a;
	x[0][1] = b;
	x[0][2] = c;

	x[1][0] = d;
	x[1][1] = e;
	x[1][2] = f;

	x[2][0] = g;
	x[2][1] = h;
	x[2][2] = i;
}

void mmult3f (matrix3f a, matrix3f b, matrix3f c) {
	a[0][0] = b[0][0]*c[0][0] + b[0][1]*c[1][0] + b[0][2]*c[2][0];
	a[0][1] = b[0][0]*c[0][1] + b[0][1]*c[1][1] + b[0][2]*c[2][1];
	a[0][2] = b[0][0]*c[0][2] + b[0][1]*c[1][2] + b[0][2]*c[2][2];

	a[1][0] = b[1][0]*c[0][0] + b[1][1]*c[1][0] + b[1][2]*c[2][0];
	a[1][1] = b[1][0]*c[0][1] + b[1][1]*c[1][1] + b[1][2]*c[2][1];
	a[1][2] = b[1][0]*c[0][2] + b[1][1]*c[1][2] + b[1][2]*c[2][2];

	a[2][0] = b[2][0]*c[0][0] + b[2][1]*c[1][0] + b[2][2]*c[2][0];
	a[2][1] = b[2][0]*c[0][1] + b[2][1]*c[1][1] + b[2][2]*c[2][1];
	a[2][2] = b[2][0]*c[0][2] + b[2][1]*c[1][2] + b[2][2]*c[2][2];
}

float mdet2f (matrix2f a) {
	return a[0][0]*a[1][1] - a[0][1]*a[1][0];
}

float mdet3f (matrix3f a) {
	matrix2f M00 = {a[1][1],a[1][2],a[2][1],a[2][2]};
	matrix2f M01 = {a[1][0],a[1][2],a[2][0],a[2][2]};
	matrix2f M02 = {a[1][0],a[1][1],a[2][0],a[2][1]};

	return a[0][0]*mdet2f(M00) - a[0][1]*mdet2f(M01) + a[0][2]*mdet2f(M02);
}

void mtranspose3f (matrix3f a, matrix3f b) {
	for(int i=0; i<3; i++) {
		for(int j=0; j<3; j++) {
			a[j][i] = b[i][j];
		}
	}
}

void vmmult3f (V3DF a, V3DF b, matrix3f c) {
	a[0] = b[0]*c[0][0] + b[1]*c[1][0] + b[2]*c[2][0];
	a[1] = b[0]*c[0][1] + b[1]*c[1][1] + b[2]*c[2][1];
	a[2] = b[0]*c[0][2] + b[1]*c[1][2] + b[2]*c[2][2];
}

void mvmult3f (V3DF a, matrix3f b, V3DF c) {
	a[0] = b[0][0]*c[0] + b[0][1]*c[1] + b[0][2]*c[2];
	a[1] = b[1][0]*c[0] + b[1][1]*c[1] + b[1][2]*c[2];
	a[2] = b[2][0]*c[0] + b[2][1]*c[1] + b[2][2]*c[2];
}




