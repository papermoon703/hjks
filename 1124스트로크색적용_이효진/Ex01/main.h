
//----------------------
#ifndef _MAIN_H_
#define _MAIN_H_
//----------------------

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#include <io.h>
#include <fcntl.h>
#include <fstream>
#include <iostream>
using namespace std;
#include <math.h>

//	OpenGL
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

//	Time recording
#include <time.h>
#include <sys/types.h>
#include <sys/timeb.h>

#define PI 3.141592653589793238462643383279f
#define E_M 2.718281828f	//�ڿ��α��� ��(base)
#define INFINITY	900000


#define NONE		0
#define LEFT		1
#define RIGHT		2


#define ON			1
#define OFF			0 

#define YES			1
#define NO			0

#define X			0
#define Y			1
#define Z			2

//����߰�
/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
#define R_			0		//color
#define G_			1
#define B_			2

//#define ROOT2	1.4142135623730950488016887242097
#define ROOT2	2.0

/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/

#define V			0
#define R			1
/*
#define BOTH		2
#define SILHOUETTE	3
*/
#define BRDR		0
#define SKLTN		1

#define MAX_CNDT_R	500

#define No_IT		1

typedef int V2DI[2];
typedef int V3DI[3];
typedef int V4DI[4];
typedef int V5DI[5];
typedef float V3DF[3];
typedef float V4DF[4];
typedef double V2DD[2];
typedef double V3DD[3];
typedef double V4DD[4];
typedef unsigned char V3DUC[3];
typedef float Matrix2DF[2][2];
typedef float Matrix3DF[3][3];
typedef float Matrix4DF[4][4];
typedef double Matrix2DD[2][2];
typedef double Matrix3DD[3][3];
typedef double Matrix4DD[4][4];

class Vertex 
{
  public:
	V3DI pt2;
	V3DI central;
};

//----------------------
#endif 
//----------------------